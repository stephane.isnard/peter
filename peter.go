package peter

import (
	"fmt"
)

// Init initialisation
func Init(titre string) {
	fmt.Println("draw mode")
	fmt.Println("say " + titre)
}

// Forward avancer de step pas
func Forward(step float64) { fmt.Printf("forward %f\n", step) }

// Forward1 avancer de 1 seul pas
func Forward1() { fmt.Println("forward 1") }

// Right tourner à droite
func Right()               { fmt.Println("right") }

// Left tourner à gauche
func Left()                { fmt.Println("left") }

// Say faire parler peter
func Say(mess string)      { fmt.Printf("say %s\n", mess) }

//Up lever le styler
func Up()                  { fmt.Println("color off") }

//Down baisser le styler
func Down(col string)      { fmt.Printf("color %s\n", col) }

//Color changer la couleur
func Color(col string)      { fmt.Printf("color %s\n", col) }

//Pivote tourner d'un certain angle (vers la droite)
func Pivote(angle int)     { fmt.Printf("right %d\n", angle) }
